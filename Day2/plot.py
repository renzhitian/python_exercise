#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 20 20:50:26 2022
This is a simple script to implement pandas to read excel data and then use matplotlib.pyplot to plot subplots.
@author: renzhi
"""
import numpy as np
import scipy
from matplotlib import pyplot as plt
import pandas as pd
import os

sheet = ('NDVI', 'SM_surf')
col = np.array([['year','initial NDVI'], ['year', 'SM_surf']])
line = np.array([['bo','b-', 'r--'], ['bo','b-', 'r--']])

arr = {}
coeff = {}
raw = {}
# plt.figure(figsize=(8.27,11.69))
fig, axes = plt.subplots(2, 1, sharex = True)
fig.set_size_inches(8.27,11.69)

for i in range(2):
    path = os.path.abspath("data.xls")
    raw[i] = pd.DataFrame(pd.read_excel(path, sheet[i]), columns = col[i]).dropna(axis = 0, how = 'any')
    print(raw[i])
    arr[i] = raw[i].to_numpy()
    coeff[i] = np.polyfit(arr[i][:, 0], arr[i][:, 1], 1)
    
    # raw[i].plot(x = col[i, 0], y = col[i, 1], kind = 'line')
    # plt.show()
    axes[i].plot(arr[i][:, 0], arr[i][:, 1], line[i, 0], markersize = 5, label = 'true value')
    axes[i].plot(arr[i][:, 0], arr[i][:, 1], line[i, 1], linewidth = 2)
    axes[i].plot(arr[i][:, 0], coeff[i][0] * arr[i][:, 0] + coeff[i][1], line[i, 2], linewidth = 2, label = 'linear-fitted value')
    axes[i].legend( loc = 'upper left') if i == 0 else None
    axes[i].set_ylim(0.34, 0.42) if i == 0 else axes[i].set_ylim(0.20, 0.28)
    axes[i].set_xticks(arr[i][::2, 0])
    plt.xticks(rotation = 45)
    axes[i].set_xlabel('year', size = 18) if i == 1 else None
    axes[i].set_ylabel(col[i, 1], size = 18)

plt.savefig('fugure.pdf')   
plt.show()
