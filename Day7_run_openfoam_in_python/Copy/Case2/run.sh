#!/bin/bash
# Tested: R. Tian - May 2022 - OF7.0

### 0. Mesh generation and solver
#checkMesh > log.checkMesh




###
blockMesh > log.blockMesh

simpleFoam > log.simpleFoam



#foamMonitor -l ./postProcessing/residuals/0/residuals.dat

# Note: `./monitor.sj` at this point, plots regularly updated convergence.

### 2. Postprocessing: Output wall-shear stresses to 'postProcessing' dir
#simpleFoam -postProcess -func wallShearStress > log.wallShearStress

### 3. Postprocessing: Sample solution according to system/sampleDict, output
###    in 'postProcessing' dir
#postProcess -func sampleDict > log.sampleDict

