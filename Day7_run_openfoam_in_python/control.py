from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile # to parse openfoam file in python
from PyFoam.Basics.DataStructures import Vector
from pathlib import Path # to convert file path to what python can operate.
import shutil # advanced python file operator package
import numpy as np 
import subprocess #to run other scripts in python

class RunOpenFoam:
    '''This is a simple script to setup, modify, and run OpenFoam in python. Make sure to use the instance method ".copy" to avoid any change to the pristine case.'''
    #TODO more methods to controling opeanfoam run will be added soon.
    def __init__(self, casedir): # Specify the directory where the OpenFoam "Case" is
        self.casedir = Path(casedir)      
    
    def copy(self, newdir): # Copy the case to avoid the any change to the pristine "Case".
        self.newdir = Path(newdir)
        print(f"Copying from {self.casedir} => {self.newdir}")
        if self.newdir.exists():
            print(f"{self.newdir} is existed, and will be overwriten")
        shutil.copytree(self.casedir, self.newdir, dirs_exist_ok=True)
        #self.casedir = self.newdir
    def get_turb_model(self):
        return ParsedParameterFile(self.newdir / "constant" / "turbulenceProperties")["RAS"]["RASModel"]
        
    def change_turb_model(self, newmodel):
        turbulenceProperties = ParsedParameterFile(self.casedir / "constant" / "turbulenceProperties")
        oldmodel = turbulenceProperties["RAS"]["RASModel"]
        print(f"Old turbulence model is {oldmodel}")
        #newmodel = newmodel
        turbulenceProperties["RAS"]["RASModel"] = newmodel
        print(f"Changing turbulence model from {oldmodel} => {newmodel}")        
        
    def change_inlet_velocity(self, U_vel):
        oldvelocity = ParsedParameterFile(self.newdir / "0" / "U")["boundaryField"]["inlet"]["value"]
        ParsedParameterFile(self.newdir / "0" / "U")["boundaryField"]["inlet"]["value"].setUniform(Vector(U_vel, 0, 0))
        print(type(ParsedParameterFile(self.newdir / "0" / "U")["boundaryField"]["inlet"]["value"]))
        ParsedParameterFile(self.newdir / "0" / "U").writeFile()
        print(f"Changing inlet velocity from {oldvelocity} => {U_vel}") 
           
    def run(self):
        exestr = f"cd {self.newdir}; ./run.sh"
        rc = subprocess.call(exestr, shell=True)
        print(f"running run.sh")
        return rc
    
    def clean(self):
        exestr = f"cd {self.newcdir}; ./clean.sh"
        rc = subprocess.call(exestr, shell=True)
        print(f"running run.sh")
        return rc
    
    def get_residuals(self):
        pass
    
    def plot_profiles(self):
        pass
        
if __name__ == "__main__":
    
    case = RunOpenFoam("pitzDaily")
    for i in range(1, 3):
        case.copy(f"./Copy/Case{i}")
        case.change_turb_model("kOmegaSST")
        case.change_inlet_velocity(f"uniform ({10*i} 0 0)")
        case.run()
        print("*****"*5)
