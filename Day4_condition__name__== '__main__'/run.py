#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 24 13:20:16 2022

@author: renzhi
"""
import module1
 
print(f'Game File: {__name__}')

def run():
    print('Game Starts')


if __name__ == '__main__':
    run()
    print(f" User's name is {module1.user}")
    print(f" User's level is {module1.level}")  