#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 26 13:06:38 2022

@author: renzhi
"""
# =============================================================================
# import random
# 
# people = ["Allen", "Michael", "John", "Ben"]
# 
# print(f"the selected element is {random.choice(people)}.")
# =============================================================================


# =============================================================================
# friends = ["Allen", "Ben", "John", "Michael", "Jim"]
# best_friend = "Allen"
# 
# if best_friend in friends:
#     print(f"{best_friend} is in your friend list!")
# =============================================================================



# =============================================================================
# budget = 103
# sandwich_price = 5
# 
# for i in range(budget // sandwich_price):
#     budget -= sandwich_price
#     print(f"you bought {i} sandwiches, you left {budget}")
# =============================================================================





# =============================================================================
# answer = []
# while True:
#     
#       ans = input("please enter the floor you want to go to")
#      
#       answer.append(ans)     
#       if answer[-1] == "out":
#           print("Exiting...")
#           break
#       elif int(answer[-1]) <= 20:
#           print(f"You are going to {ans} floor")
#           if len(answer) > 1 and int(answer[-1]) == int(answer[-2]):
#               print(f"You are already on the {ans} floor")
#           else: pass
#       else:
#           print("Please enter a number <= 20")
#           #answer.clear()
# =============================================================================





# =============================================================================
# def special_count(start, end, step = 1):
#     for i in range(start, end):
#         print(f"counting...{i}")
# 
#         
# special_count(1, 101)
# print("****"*3)
# 
# =============================================================================


# =============================================================================
# class CreditCard:
#     scale = 1.5
#     def __init__(self, number, limit, company):
#         print('I am created')
#         self.number = number
#         self.limit = limit
#         self.company = company       
#     pass
#     def hide(self):
#         self.number = f"**** **** **** {self.number[-4:]}"
#         return self.number
#     def raised_limit(self):
#         self.limit = self.limit * CreditCard.scale
#         return self.limit
# name = "Jim"    def add_points(self)

# my_card = CreditCard("1739179344465789", 1000, 'renzhi')
# your_card = CreditCard("1730808081246756", 1000, 'xixi')
# 
# print(my_card.raised_limit())
# print(my_card.hide())
# print(your_card.number)
# print(type(name))
# print(type(my_card.number))
# print(type(your_card.number))
# =============================================================================



# =============================================================================
# two players card game:
# import random
        
# total = 5
# p1 = list(range(1, total + 1))
# p2 = list(range(1, total + 1))
# print("Game start...")
# count_1 = 0
# count_2 = 0
# while True:
#     if len(p1) == 0 or len(p2) == 0:
#         print("Game over!")
#         break
#     flag = input("Click Enter to pick cards!")
#     # if flag != "\n":
#     #     print("Please press the 'Enter'!")
#     #     continue
    
#     picked1 = random.choice(p1)
#     picked2 = random.choice(p2)
#     print(f"p1 card is {picked1}")
#     print(f"p2 card is {picked2}")
#     if picked1 > picked2:
#         print(f"A point is added to p1") 
#         count_1 += 1
#     elif picked1 < picked2:  
#         print(f"A point is added to p2") 
#         count_2 += 1
#     else:
#         print("TIE")
        
#     print(f"Score: {count_1} - {count_2}")
#     print("-------------"*3)
#     p1.remove(picked1)
#     p2.remove(picked2)
# =============================================================================






# two players card game:
    
import random    

total = 10
class Players:
    def __init__(self, name):
        self.name = name
        self.points = 0
        self.cards = list(range(1, total+1))
        random.shuffle(self.cards)
        
    def pick_card(self):
        picked_card = self.cards[0]
        self.cards.remove(picked_card)
        print(f"{self.name} is {picked_card}")
        return picked_card
    
    def add_point(self):
        print(f"A point is added to {self.name}")
        
    def is_game_over(self):
        return len(self.cards) == 0 or self.points > total/2 + 1
        
 
print("Game start...")
p1 = Players(name = "Player 1")
p2 = Players(name = "Player 2")
while True:
    input("Click Enter to pick cards")
    p1_card = p1.pick_card()
    p2_card = p2.pick_card()
    if p1_card > p2_card:
        p1.add_point()
        p1.points += 1
    elif p1_card < p2_card:
        p2.add_point()
        p2.points += 1
    else:
        print("Tie!")
    print(f"Score: {p1.points} - {p2.points}")
    print("******"*5)
    if p1.is_game_over() or p2.is_game_over():
        print ("Game over!")
        if p1.points > p2.points:
            print(f"{p1.name} win!")
        elif p1.points < p2.points:
            print(f"{p2.name} win!")
        else:
            print("TIE!")
        print(f"Final score is {p1.points} - {p2.points}")
        break
    







