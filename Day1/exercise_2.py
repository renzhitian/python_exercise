#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 20 14:18:43 2022

@author: renzhi

"""

import numpy as np
import random

"""
This is a script to output the average of 
the array at each line without its min and max in multiple files.
Input: raw_*.txt
Output: aver_*txt
"""
###Generate data
raw = {}
aver = {}
for i in range(5):
    tep = np.arange(i*1000,i*1000+1000)
    random.shuffle(tep)
    raw[i] = tep.reshape((100, 10))
    ##write raw files to be read later.
    np.savetxt(f"raw_{i}.txt", raw[i], header=f'An array with shape: {raw[i].shape}', fmt = "%.9e", delimiter ="  ")  
    
##read and average data excluding the extrema   
for i in range(5):
    with open(f'raw_{i}.txt', 'r') as fh:
        data = np.loadtxt(fh, skiprows = 1)  
    
    aver[i] = np.mean(np.sort(data, axis = 1)[:, 1:-1], axis = 1)
    
    ##write average results files.
    np.savetxt(f"aver_{i}.txt", aver[i], header=f'Average of file raw_{i}', fmt = "%.9e", delimiter ="  ")


    
    
    