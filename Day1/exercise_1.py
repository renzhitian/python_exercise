#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 20 14:18:43 2022

@author: renzhi

"""

import numpy as np
import random

"""
This is a script to output the average of 
the array at each line without its min and max.
"""
###Generate data

raw = np.arange(1000)
random.shuffle(raw)
raw.shape = 100, 10


##write data into file
# =============================================================================
# with open('./raw.txt','w') as fw:
#     fw.write(f'# An array with shape: {raw.shape}\n')
#     for i in range(raw.shape[0]):
#         for j in range(raw.shape[1]):
#             fw.write(f'{raw[i,j]}   ')
#         fw.write(f'\n')
# =============================================================================  


np.savetxt("raw.txt", raw, header=f'An array with shape: {raw.shape}', fmt = "%.9e", delimiter ="  ")  
    
##read and average data excluding the extrema   
with open(f'raw.txt', 'r') as fh:
    data = np.loadtxt(fh, skiprows = 1)  
    
aver = np.mean(np.sort(data, axis = 1)[:, 1:-1], axis = 1)

    
    
    